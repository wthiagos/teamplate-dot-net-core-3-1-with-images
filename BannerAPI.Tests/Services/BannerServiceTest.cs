using System;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using BannerAPI.Entities;
using BannerAPI.Helpers;
using BannerAPI.Services;
using Xunit;

namespace BannerAPI.Tests.Services
{
    public class BannerServiceTest : IDisposable
    {
        private readonly BannerService BannerService;
        private readonly DbContextOptions<DataContext> Options;
        private readonly SqliteConnection Connection;
        private readonly Banner Banner = new Banner
        {
            Id = 1,
            DataCadastro = DateTime.Now,
            Descricao = "Campanha contra o cancer",
            NomeImagem = "",
            LinkExterno = false,
            Rota = "/campanha/cancer",
            Titulo = "Faça seus exames",
            IdUsuario = 1,
        };

        public BannerServiceTest()
        {
            Connection = new SqliteConnection("DataSource=:memory:");
            Connection.Open();

            Options = new DbContextOptionsBuilder<DataContext>()
                    .UseSqlite(Connection)
                    .Options;

            using (DataContext context = new DataContext(Options))
                context.Database.EnsureCreated();

            BannerService = new BannerService(new DataContext(Options));
        }

        [Fact]
        public async Task ListaSucesso()
        {
            await BannerService.Criar(Banner);

            var result = await BannerService.Listar();
            Assert.NotEmpty(result);
        }

        [Fact]
        public async Task BuscaErro()
        {
            await BannerService.Criar(Banner);

            await Assert.ThrowsAsync<AppException>(() => BannerService.Buscar(999));
        }

        [Fact]
        public async Task BuscaErroZero()
        {
            await BannerService.Criar(Banner);

            await Assert.ThrowsAsync<AppException>(() => BannerService.Buscar(0));
        }

        [Fact]
        public async Task BuscaSucesso()
        {
            await BannerService.Criar(Banner);

            var result = await BannerService.Buscar(1);
            Assert.NotNull(result);
        }

        [Fact]
        public async Task CriarSucesso()
        {
            await BannerService.Criar(Banner);
            return;
        }

        [Fact]
        public async Task CriarErro()
        {
            await Assert.ThrowsAsync<AppException>(() => BannerService.Criar(null));
        }

        [Fact]
        public async Task AtualizarSucesso()
        {
            await BannerService.Criar(Banner);
            await BannerService.Atualizar(Banner);
            return;
        }

        [Fact]
        public async Task AtualizarErro()
        {
            await BannerService.Criar(Banner);
            await Assert.ThrowsAsync<AppException>(() => BannerService.Atualizar(null));
        }

        [Fact]
        public async Task ExcluirSucesso()
        {
            await BannerService.Criar(Banner);
            await BannerService.Excluir(1);
            return;
        }

        [Fact]
        public async Task ExcluirErro()
        {
            await BannerService.Criar(Banner);
            await Assert.ThrowsAsync<AppException>(() => BannerService.Excluir(999));
        }

        [Fact]
        public async Task ExcluirErroZero()
        {
            await BannerService.Criar(Banner);
            await Assert.ThrowsAsync<AppException>(() => BannerService.Excluir(0));
        }

        public void Dispose()
        {
            Connection.Close();
        }
    }
}
