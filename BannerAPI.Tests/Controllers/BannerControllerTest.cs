using Xunit;
using System;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.Extensions.Options;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using BannerAPI.Helpers;
using BannerAPI.Entities;
using BannerAPI.Models.Banner;
using BannerAPI.Services;
using BannerAPI.Controllers;
using Microsoft.AspNetCore.Http;
using System.Net.Http;

namespace BannerAPI.Tests.Controllers
{
    public class BannerControllerTest : IDisposable
    {
        private readonly BannerService BannerService;
        private readonly IMapper Mapper;
        private readonly IOptions<AppSettings> AppSettings;
        private readonly DbContextOptions<DataContext> DbOptions;
        private readonly SqliteConnection Connection;
        private readonly IConfiguration Configuration;
        private readonly FileStream Stream;
        private readonly Banner Banner = new Banner
        {
            Id = 1,
            DataCadastro = DateTime.Now,
            Descricao = "Campanha contra o cancer",
            NomeImagem = "",
            LinkExterno = false,
            Rota = "/campanha/cancer",
            Titulo = "Faça seus exames",
            IdUsuario = 1,
        };


        private readonly BannerCreateModel BannerCreateModel = new BannerCreateModel
        {
            CaminhoImagem = "",
            Subtitulo = "",
            Descricao = "Campanha contra o cancer",
            NomeImagem = "",
            LinkExterno = false,
            Rota = "/campanha/cancer",
            Titulo = "Faça seus exames",
        };

        private readonly BannerUpdateModel BannerUpdateModel = new BannerUpdateModel
        {
            Id = 1,
            Descricao = "Campanha contra o cancer",
            NomeImagem = "",
            LinkExterno = false,
            Rota = "/campanha/cancer",
            Titulo = "Faça seus exames",
        };

        public BannerControllerTest()
        {
            AutoMapperProfile mapperProfile = new AutoMapperProfile();
            Connection = new SqliteConnection("DataSource=:memory:");
            Connection.Open();

            DbOptions = new DbContextOptionsBuilder<DataContext>()
                    .UseSqlite(Connection)
                    .Options;

            using (DataContext context = new DataContext(DbOptions))
                context.Database.EnsureCreated();

            BannerService = new BannerService(new DataContext(DbOptions));
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            IConfigurationSection appSettingsSection = Configuration.GetSection("AppSettings");

            var appSettings = appSettingsSection.Get<AppSettings>();

            AppSettings = Options.Create<AppSettings>(appSettings);

            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(mapperProfile);
            });

            Mapper = config.CreateMapper();

            Stream = File.OpenRead("src/cerebro.jpg");
            BannerCreateModel.Arquivo = new FormFile(Stream, 0, Stream.Length, null, Path.GetFileName(Stream.Name))
            {
                Headers = new HeaderDictionary(),
                ContentType = "application/pdf"
            };

            BannerUpdateModel.Arquivo = new FormFile(Stream, 0, Stream.Length, null, Path.GetFileName(Stream.Name))
            {
                Headers = new HeaderDictionary(),
                ContentType = "application/pdf"
            };

            AppSettings.Value.PathToSave = Directory.GetCurrentDirectory() + "\\";
            AppSettings.Value.PathToGet = Directory.GetCurrentDirectory() + "\\";
        }

        [Fact]
        public void ConstrutorSucesso()
        {
            var result = new BannerController(BannerService, Mapper, AppSettings);
            Assert.NotNull(result);
        }

        [Fact]
        public async void ListarSucesso()
        {
            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            var result = await controller.Get();
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void BuscarSucesso()
        {
            await BannerService.Criar(Banner);
            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            IActionResult result = await controller.GetById(1);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void BuscarErro()
        {
            await BannerService.Criar(Banner);
            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.GetById(999));
        }

        [Fact]
        public async void BuscarErroZero()
        {
            await BannerService.Criar(Banner);
            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.GetById(0));
        }

        [Fact]
        public async void CriarSucesso()
        {
            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            IActionResult result = await controller.Post(BannerCreateModel);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async void CriarSucessoDiretorioNovo()
        {
            AppSettings.Value.PathToSave += new Guid() + "\\";

            if (Directory.Exists(AppSettings.Value.PathToSave))
                Directory.Delete(AppSettings.Value.PathToSave, true);

            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            IActionResult result = await controller.Post(BannerCreateModel);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async void CriarErro()
        {
            var connection = new SqliteConnection("");
            connection.Open();
            var dbOptions = new DbContextOptionsBuilder<DataContext>()
                    .UseSqlite(connection)
                    .Options;
            var bannerService = new BannerService(new DataContext(dbOptions));

            BannerController controller = new BannerController(bannerService, Mapper, AppSettings);
            IActionResult result = await controller.Post(BannerCreateModel);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void CriarErroNulo()
        {
            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.Post(null));
        }

        [Fact]
        public async void AtualizarSucesso()
        {
            var c = new BannerController(BannerService, Mapper, AppSettings);
            await c.Post(BannerCreateModel);

            using (DataContext context = new DataContext(DbOptions))
            {
                BannerService service = new BannerService(context);
                BannerController controller = new BannerController(service, Mapper, AppSettings);
                IActionResult result = await controller.Put(BannerUpdateModel);
                Assert.IsType<OkResult>(result);
            }
        }

        [Fact]
        public async void AtualizarSucessoNovoDiretorio()
        {
            AppSettings.Value.PathToSave += new Guid() + "\\";

            if (Directory.Exists(AppSettings.Value.PathToSave))
                Directory.Delete(AppSettings.Value.PathToSave, true);

            var c = new BannerController(BannerService, Mapper, AppSettings);
            await c.Post(BannerCreateModel);

            using (DataContext context = new DataContext(DbOptions))
            {
                BannerService service = new BannerService(context);
                BannerController controller = new BannerController(service, Mapper, AppSettings);
                IActionResult result = await controller.Put(BannerUpdateModel);
                Assert.IsType<OkResult>(result);
            }
        }

        [Fact]
        public async void AtualizarSucessoArquivoNulo()
        {
            var c = new BannerController(BannerService, Mapper, AppSettings);
            await c.Post(BannerCreateModel);

            using (DataContext context = new DataContext(DbOptions))
            {
                BannerService service = new BannerService(context);
                BannerController controller = new BannerController(service, Mapper, AppSettings);
                BannerUpdateModel.Arquivo = null;
                IActionResult result = await controller.Put(BannerUpdateModel);
                Assert.IsType<OkResult>(result);
            }
        }

        [Fact]
        public async void AtualizarErro()
        {
            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            IActionResult result = await controller.Put(BannerUpdateModel);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void AtualizarErroNulo()
        {
            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.Put(null));
        }

        [Fact]
        public async void ExcluirSucesso()
        {
            var c = new BannerController(BannerService, Mapper, AppSettings);
            await c.Post(BannerCreateModel);

            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            IActionResult result = await controller.Delete(1);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async void ExcluirErro()
        {
            await BannerService.Criar(Banner);
            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.Delete(999));
        }

        [Fact]
        public async void ExcluirErroZero()
        {
            await BannerService.Criar(Banner);
            BannerController controller = new BannerController(BannerService, Mapper, AppSettings);
            await Assert.ThrowsAsync<AppException>(() => controller.Delete(0));
        }

        public void Dispose()
        {
            Stream.Close();
            Connection.Close();
        }
    }
}
