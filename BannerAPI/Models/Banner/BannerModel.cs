//===============================================================================
//Web API Banner
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Banner 
//==============================================================================

using System;
using System.Diagnostics.CodeAnalysis;

namespace BannerAPI.Models.Banner
{
    [ExcludeFromCodeCoverage]
    public class BannerModel
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Descricao { get; set; }
        public int IdUsuario { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Rota { get; set; }
        public bool LinkExterno { get; set; }
        public string CaminhoImagem { get; set; }
        public string NomeImagem { get; set; }
    }
}