//===============================================================================
//Web API Banner
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Banner 
//==============================================================================

using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Http;

namespace BannerAPI.Models.Banner
{
    [ExcludeFromCodeCoverage]
    public class BannerUpdateModel
    {
        [Required]
        public int? Id { get; set; }
        [StringLength(100)]
        [Required]
        public string Titulo { get; set; }
        [StringLength(100)]
        public string Subtitulo { get; set; }
        [StringLength(255)]
        public string Descricao { get; set; }
        [Required]
        [StringLength(100)]
        public string Rota { get; set; }
        [Required]
        public bool LinkExterno { get; set; }
        public IFormFile Arquivo { get; set; }
        [StringLength(255)]
        public string CaminhoImagem { get; set; }
        [StringLength(255)]
        public string NomeImagem { get; set; }
    }
}