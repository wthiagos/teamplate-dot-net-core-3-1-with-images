//===============================================================================
//Web API Banner
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Banner 
//==============================================================================

using BannerAPI.Helpers;
using System.Linq;

namespace BannerAPI.Services
{
    public interface IUsuarioService
    {
        bool Validar(int id);
    }

    public class UsuarioService : IUsuarioService
    {
        private readonly DataContext Db;

        ///<summary>
        ///
        ///Esse método serve para atribuir os objetos recebidos para injeção de depencia
        ///para os atributos da classe
        ///
        ///</summary>
        ///<param name="db">Contexto do banco de dados</param>
        public UsuarioService(DataContext db)
        {
            Db = db;
        }

        ///<summary>
        ///
        ///Esse método serve para validar se o Id do usuário consta na base de dados
        ///
        ///</summary>
        ///<param name="id">Id do usuário</param>
        public bool Validar(int id)
        {
            if (id == 0)
                throw new AppException("O id do usuário não pode ser igual a 0");

            return Db.Usuario.Any(u => u.Id == id);
        }
    }
}