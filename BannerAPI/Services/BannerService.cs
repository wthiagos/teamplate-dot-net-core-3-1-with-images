//===============================================================================
//Web API Banner
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Banner 
//==============================================================================

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BannerAPI.Entities;
using BannerAPI.Helpers;
using Microsoft.EntityFrameworkCore;

namespace BannerAPI.Services
{
    public interface IBannerService
    {
        Task<List<Banner>> Listar();
        Task<Banner> Buscar(int id);
        Task Criar(Banner model);
        Task Atualizar(Banner model);
        Task Excluir(int id);
    }

    public class BannerService : IBannerService
    {
        private readonly DataContext Db;

        ///<summary>
        ///
        ///Esse método serve para atribuir os objetos recebidos para injeção de depencia
        ///para os atributos da classe
        ///
        ///</summary>
        ///<param name="db">Contexto do banco de dados</param>
        public BannerService(DataContext db)
        {
            Db = db;
        }

        ///<summary>
        ///
        ///Esse método serve para listar todos os banners da base.
        ///
        ///</summary>
        public async Task<List<Banner>> Listar()
        {
            return await Db.Set<Banner>().ToListAsync();
        }

        ///<summary>
        ///
        ///Esse método serve para buscar um banner por Id.
        ///
        ///</summary>
        ///<param name="id">Id do banner</param>
        public async Task<Banner> Buscar(int id)
        {
            if (id == 0)
                throw new AppException("O id não pode ser igual a 0");

            Banner banner = await Db.Set<Banner>().AsNoTracking().FirstOrDefaultAsync(m => m.Id == id);

            if (banner == null)
                throw new AppException("Banner não encontrado");

            return banner;
        }

        ///<summary>
        ///
        ///Esse método serve inserir um banner na base.
        ///
        ///</summary>
        ///<param name="banner">Model de banner para ser inserido</param>
        public async Task Criar(Banner banner)
        {
            if (banner == null)
                throw new AppException("O banner não pode estar nulo");

            await Db.Set<Banner>().AddAsync(banner);
            await Db.SaveChangesAsync();
        }

        ///<summary>
        ///
        ///Esse método serve atualizar um banner na base.
        ///
        ///</summary>
        ///<param name="banner">Model de banner para ser atualizado</param>
        public async Task Atualizar(Banner banner)
        {
            if (banner == null)
                throw new AppException("O banner não pode estar nulo");

            Db.Set<Banner>().Update(banner);

            await Db.SaveChangesAsync();
        }

        ///<summary>
        ///
        ///Esse método serve excluir um banner na base.
        ///
        ///</summary>
        ///<param name="id">Id do banner a ser excluído</param>
        public async Task Excluir(int id)
        {
            if (id == 0)
                throw new AppException("O id não pode ser igual a 0");

            Banner entity = await Db.Set<Banner>().FirstOrDefaultAsync(m => m.Id == id);

            if (entity != null)
            {
                Db.Set<Banner>().Remove(entity);

                await Db.SaveChangesAsync();
            }
            else
            {
                throw new AppException("Banner não encontrado");
            }
        }
    }
}