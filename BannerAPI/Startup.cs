//===============================================================================
//Web API Banner
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Banner 
//==============================================================================

using System;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BannerAPI.Helpers;
using BannerAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;
using NoticiaAPI.Helpers;

namespace BannerAPI
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        private readonly IWebHostEnvironment Env;
        private readonly IConfiguration Configuration;
        public static string ConnectionString { get; private set; }

        ///<summary>
        ///
        ///Esse método construtor é utilizado para pegar os objetos de injeção de dependencia
        ///e atribuir aos objetos da classe.
        ///
        ///</summary>
        ///<param name="env">Ambiente em que a API está sendo executada, ex(Develop, Release, Production)</param>
        ///<param name="configuration">Configurações da API</param>
        public Startup(IWebHostEnvironment env, IConfiguration configuration)
        {
            Env = env;
            Configuration = configuration;
            ConnectionString = Configuration.GetConnectionString("SqlServer");
        }

        ///<summary>
        ///
        ///Esse método serve para configurar tudo que a API precisará inicialmente
        ///para seu total funcionamento.
        ///
        ///</summary>
        ///<param name="services">Coleção de serviços que serão utilizados na API, junto com suas configurações</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>();

            services.AddCors();
            services.AddControllers();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            IConfigurationSection appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            AppSettings appSettings = appSettingsSection.Get<AppSettings>();
            byte[] key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        IUsuarioService userService = context.HttpContext.RequestServices.GetRequiredService<IUsuarioService>();
                        int userId = int.Parse(context.Principal.Identity.Name);
                        HttpUser.IdUsuario = userId;

                        if (!userService.Validar(userId))
                            context.Fail("Unauthorized");

                        return Task.CompletedTask;
                    }
                };
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddScoped<IUsuarioService, UsuarioService>();
            services.AddScoped<IBannerService, BannerService>();
        }

        ///<summary>
        ///
        ///Esse método serve para configurar a parte de REST da APi.
        ///
        ///</summary>
        ///<param name="app">Objeto de aplicação para configurar a parte de REST</param>
        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseExceptionHandler(a => a.Run(async context =>
            {
                IExceptionHandlerPathFeature exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                Exception exception = exceptionHandlerPathFeature.Error;
                string result = ErrorHandler.TreatError(exception);

                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(result);
            }));

            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
