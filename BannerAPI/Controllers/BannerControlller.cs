//===============================================================================
//Web API Banner
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Banner 
//==============================================================================

using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using BannerAPI.Services;
using AutoMapper;
using BannerAPI.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using BannerAPI.Entities;
using BannerAPI.Models.Banner;
using System.IO;
using System;

namespace BannerAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class BannerController : ControllerBase
    {
        private readonly IBannerService BannerService;
        private readonly IMapper Mapper;
        private readonly AppSettings AppSettings;

        ///<summary>
        ///
        ///Esse método construtor é utilizado para pegar os objetos de injeção de dependencia
        ///e atribuir aos objetos da classe.
        ///
        ///</summary>
        ///<param name="bannerService">Serviço de banner para consumo do banco de dados</param>
        ///<param name="mapper">Mapeador de objetos</param>
        ///<param name="appSettings">Configurações da aplicação</param>
        public BannerController(
            IBannerService bannerService,
            IMapper mapper,
            IOptions<AppSettings> appSettings
        )
        {
            BannerService = bannerService;
            Mapper = mapper;
            AppSettings = appSettings.Value;
        }

        ///<summary>
        ///
        ///Esse método serve para listar todos banners do banco de dados e
        ///mapear esse objeto para um objeto de retorno mais simples.
        ///Esse método pode ser acessado sem estar logado e é preciso ser um tipo de requisão GET.
        ///
        ///</summary>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            List<Banner> banners = await BannerService.Listar();

            List<BannerModel> model = Mapper.Map<List<BannerModel>>(banners);

            return Ok(model);
        }

        ///<summary>
        ///
        ///Esse método serve para buscar um banner através do Id e
        ///mapear esse objeto para um objeto de retorno mais simples.
        ///Esse método não pode ser acessado sem estar logado e é preciso ser um tipo de requisão GET.
        ///
        ///</summary>
        ///<param name="Id">Id do banner</param>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id == 0)
                throw new AppException("O id não pode ser igual a 0");

            Banner banner = await BannerService.Buscar(id);

            BannerModel model = Mapper.Map<BannerModel>(banner);

            return Ok(model);
        }

        ///<summary>
        ///
        ///Esse método serve para inserir um banner na base, primeiro mapeando
        ///o objeto recebido para o objeto esperado na base.
        ///Esse método não pode ser acessado sem estar logado e é preciso ser um tipo de requisão POST.
        ///
        ///</summary>
        ///<param name="model">Model de criação de um banner</param>
        [HttpPost]
        public async Task<IActionResult> Post([FromForm] BannerCreateModel model)
        {
            if (model == null)
                throw new AppException("O banner não pode estar nulo");

            string fileName = Path.GetFileNameWithoutExtension(model.Arquivo.FileName);
            fileName = fileName.ToLower().Replace(' ', '_');
            string fileType = Path.GetExtension(model.Arquivo.FileName);
            string path = $"{AppSettings.PathToSave}{fileName}_{DateTime.Now.ToString("yyyyMMddHHmmssfffffff")}{fileType}";
            string directoryName = Path.GetDirectoryName(path);

            try
            {
                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                using (var fileStream = new System.IO.FileStream(path, FileMode.Create))
                {
                    await model.Arquivo.CopyToAsync(fileStream);
                }

                model.NomeImagem = model.Arquivo.FileName;
                model.CaminhoImagem = path.Replace(AppSettings.PathToSave, AppSettings.PathToGet);

                Banner banner = Mapper.Map<Banner>(model);

                await BannerService.Criar(banner);

                return Ok();
            }
            catch (System.Exception ex)
            {
                if (System.IO.File.Exists(path))
                    System.IO.File.Delete(path);

                return BadRequest(new
                {
                    message = ex.Message
                });
            }
        }

        ///<summary>
        ///
        ///Esse método serve para atualizar um banner na base, primeiro mapeando
        ///o objeto recebido para o objeto esperado na base.
        ///Esse método pode ser acessado sem estar logado e é preciso ser um tipo de requisão PUT.
        ///
        ///</summary>
        ///<param name="model">Model de atualização de um banner</param>
        [HttpPut]
        public async Task<IActionResult> Put([FromForm] BannerUpdateModel model)
        {
            if (model == null)
                throw new AppException("O banner não pode estar nulo");

            try
            {
                Banner banner = await BannerService.Buscar(model.Id.Value);

                if (model.Arquivo != null)
                {
                    if (System.IO.File.Exists(banner.CaminhoImagem))
                        System.IO.File.Delete(banner.CaminhoImagem);

                    string fileName = Path.GetFileNameWithoutExtension(model.Arquivo.FileName);
                    fileName = fileName.ToLower().Replace(' ', '_');
                    string fileType = Path.GetExtension(model.Arquivo.FileName);
                    string path = $"{AppSettings.PathToSave}{fileName}_{DateTime.Now.ToString("yyyyMMddHHmmssfffffff")}{fileType}";
                    string directoryName = Path.GetDirectoryName(path);

                    using (var fileStream = new System.IO.FileStream(path, FileMode.Create))
                    {
                        await model.Arquivo.CopyToAsync(fileStream);
                    }

                    model.NomeImagem = model.Arquivo.FileName;
                    model.CaminhoImagem = path.Replace(AppSettings.PathToSave, AppSettings.PathToGet);
                }
                else
                {
                    model.CaminhoImagem = banner.CaminhoImagem;
                    model.NomeImagem = banner.NomeImagem;
                }

                banner = Mapper.Map<Banner>(model);

                await BannerService.Atualizar(banner);

                return Ok();
            }
            catch (System.Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        ///<summary>
        ///
        ///Esse método serve para excluir um banner na base.
        ///Esse método pode ser acessado sem estar logado e é preciso ser um tipo de requisão DELETE.
        ///
        ///</summary>
        ///<param name="id">Id do banner</param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
                throw new AppException("O id não pode ser igual a 0");

            Banner banner = await BannerService.Buscar(id);

            if (System.IO.File.Exists(banner.CaminhoImagem))
                System.IO.File.Delete(banner.CaminhoImagem);

            await BannerService.Excluir(id);

            return Ok();
        }
    }
}
