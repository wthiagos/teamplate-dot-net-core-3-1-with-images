//===============================================================================
//Web API Banner
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Banner 
//==============================================================================

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace BannerAPI.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("Banner")]
    public class Banner
    {
        [Column("Id")]
        public int Id { get; set; }
        [Column("Titulo")]
        public string Titulo { get; set; }
        [Column("Subtitulo")]
        public string Subtitulo { get; set; }
        [Column("Descricao")]
        public string Descricao { get; set; }
        [Column("IdUsuario")]
        public int IdUsuario { get; set; }
        [Column("DataCadastro")]
        public DateTime DataCadastro { get; set; }
        [Column("Rota")]
        public string Rota { get; set; }
        [Column("LinkExterno")]
        public bool LinkExterno { get; set; }
        [Column("CaminhoImagem")]
        public string CaminhoImagem { get; set; }
        [Column("NomeImagem")]
        public string NomeImagem { get; set; }
    }
}