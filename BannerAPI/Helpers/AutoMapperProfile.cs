//===============================================================================
//Web API Banner
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Banner 
//==============================================================================

using System.Diagnostics.CodeAnalysis;
using AutoMapper;
using BannerAPI.Entities;
using BannerAPI.Models.Banner;

namespace BannerAPI.Helpers
{
    [ExcludeFromCodeCoverage]
    public class AutoMapperProfile : Profile
    {
        ///<summary>
        ///
        ///Esse método serve para mapear os objetos de entidades com suas models
        ///
        ///</summary>
        public AutoMapperProfile()
        {
            CreateMap<BannerUpdateModel, Banner>();
            CreateMap<BannerCreateModel, Banner>();
            CreateMap<Banner, BannerModel>();
        }
    }
}