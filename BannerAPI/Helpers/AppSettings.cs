//===============================================================================
//Web API Banner
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Banner 
//==============================================================================

using System.Diagnostics.CodeAnalysis;

namespace BannerAPI.Helpers
{
    [ExcludeFromCodeCoverage]
    public class AppSettings
    {
        public string Secret { get; set; }
        public string PathToSave { get; set; }
        public string PathToGet { get; set; }
    }
}