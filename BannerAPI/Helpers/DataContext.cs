//===============================================================================
//Web API Banner
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Banner 
//==============================================================================

using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using BannerAPI.Entities;
using NoticiaAPI.Helpers;

namespace BannerAPI.Helpers
{
    [ExcludeFromCodeCoverage]
    public class DataContext : DbContext
    {
        ///<summary>
        ///
        ///Esse método serve para iniciar a instancia do construtor ja recebendo suas configurações iniciais.
        ///
        ///</summary>
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }

        ///<summary>
        ///
        ///Esse método serve para configurar o contexto do banco de dados a ser utilizado na aplicação.
        ///Caso não esteja configurado, irá usar banco em memória para testes unitários
        ///
        ///</summary>
        ///<param name="optionsBuilder">Objeto de configuração do contexto do banco</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer(Startup.ConnectionString);
        }

        ///<summary>
        ///
        ///Esse método serve para configurar a parte das models do banco de dados
        ///
        ///</summary>
        ///<param name="modelBuilder">Objeto de configuração de models do banco</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");
        }

        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Banner> Banner { get; set; }

        public override int SaveChanges()
        {
            var changedEntities = ChangeTracker
                .Entries()
                .Where(_ => _.State == EntityState.Added ||
                            _.State == EntityState.Modified);

            foreach (var e in changedEntities)
            {
                PropertyInfo data = e.Entity.GetType().GetProperty("DataCadastro");
                PropertyInfo idUsuario = e.Entity.GetType().GetProperty("IdUsuario");

                if (e.State == EntityState.Added && data != null)
                {
                    data.SetValue(e.Entity, DateTime.Now, null);
                }

                if (e.State == EntityState.Modified && data != null)
                {
                    base.Entry(e.Entity).Property("DataCadastro").IsModified = false;
                }

                if (idUsuario != null && HttpUser.IdUsuario != null)
                {
                    idUsuario.SetValue(e.Entity, HttpUser.IdUsuario, null);
                }
            }

            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var changedEntities = ChangeTracker
                .Entries()
                .Where(_ => _.State == EntityState.Added ||
                            _.State == EntityState.Modified);

            foreach (var e in changedEntities)
            {
                PropertyInfo data = e.Entity.GetType().GetProperty("DataCadastro");
                PropertyInfo idUsuario = e.Entity.GetType().GetProperty("IdUsuario");

                if (e.State == EntityState.Added && data != null)
                {
                    data.SetValue(e.Entity, DateTime.Now, null);
                }

                if (e.State == EntityState.Modified && data != null)
                {
                    base.Entry(e.Entity).Property("DataCadastro").IsModified = false;
                }

                if (idUsuario != null && HttpUser.IdUsuario != null)
                {
                    idUsuario.SetValue(e.Entity, HttpUser.IdUsuario, null);
                }
            }
            return (await base.SaveChangesAsync(true, cancellationToken));
        }
    }
}